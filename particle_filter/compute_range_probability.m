function p= compute_range_probability(z,z_expected,laser_spec,sensor_model_spec)
  %the output is mxL
  %L is the laser scan size
  %z is observation at time t 1xL
  %z_expected is the expected observation from m particles mxL
  % senosr_model_spec - used to generate probability distribution p(z|x)
  %z_=repmat(z,length(z_expected),1);
  p1=hit_probability(z,z_expected,sensor_model_spec.hit_sigma);
  p2=short_probability(z,z_expected,sensor_model_spec.short_lambda);
  p3=max_probability(z,laser_spec.max_range);
  p4=noise_probability(z,laser_spec.max_range);
  %[p1' p2' p3' p4']
  p=bsxfun(@plus,sensor_model_spec.w_max*p3,sensor_model_spec.w_hit*p1)+bsxfun(@plus,sensor_model_spec.w_noise*p4,sensor_model_spec.w_short*p2);
end

function p=hit_probability(z,z_expected,sigma)
  % hit is represented by a gaussian distribution
  % sigma is the sqrt of variance of this distribution
  % z is actual measurement received
  % z_expected is the expected range measurement based on particle position
  % z_max is max laser range
  z_=bsxfun(@minus,z, z_expected);
  p=(1/(sigma*sqrt(2*pi)))*exp(-z_.^2/(2*sigma^2));
end

function p=short_probability(z,z_expected,lambda)
  % short is represented by a exponential distribution
  % lambda represents this distribution
  % z is actual measurement received
  % z_expected is the expected range measurement based on particle position
  z_=repmat(z,size(z_expected,1),1);
  p=lambda*exp(-z_*lambda);
  p(z_>z_expected)=0;
  p(z_<0)=0;
  %making sure probability integrates to one
  %hack: removing 0 range values to keep normalizer non-zero
  z_expected(z_expected==0)=1;
  p=p./(1 - exp(-z_expected*lambda));
end

function p=max_probability(z,z_max)
  % max return is represented by a uniform distribution
  % z is actual measurement received
  % z_expected is the expected range measurement based on particle position
  % z_max is max laser range
  %z_=repmat(z,length(z_expected),1);
  p=zeros(size(z));
  p(z==z_max)=1;
end

function p=noise_probability(z,z_max)
  % noise is represented by a uniform distribution
  % z is actual measurement received
  % z_expected is the expected range measurement based on particle position
  % z_max is max laser range
  %z_=repmat(z,length(z_expected),1);
  p=ones(size(z))/z_max;
  p(z<0)=0;
  p(z>z_max)=0;

end


