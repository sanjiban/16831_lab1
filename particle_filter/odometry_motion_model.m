function new_particles=odometry_motion_model(particles,oldpose,newpose,odomparam,map,free_threshold)
% move particles according to odometry motion model.
%  particles: contain all the particle weighs and poses.
%  oldpose: last odometry pose  [x y theta]   N*3
%  newpose: current odometry pse [x y theta]  1*3
%  odomparam: contain four parameters of motion model noise

delta_x = newpose(1) - oldpose(1);
delta_y = newpose(2) - oldpose(2);
delta_rot = newpose(3) - oldpose(3);

rot1 = atan2(delta_y, delta_x) - oldpose(3);
trans = sqrt((delta_x)^2 + (delta_y)^2);
rot2 = delta_rot - rot1;

rot1=shift_angles(rot1);
rot2=shift_angles(rot2);

a=odomparam;
numP = size(particles.allposes, 1);
noisy_rot1 = repmat(rot1, numP, 1)  - randn(numP,1) * sqrt( a(1)*rot1^2 + a(2)*trans^2 );   %generate samples with variance sqrt(...)
noisy_trans = repmat(trans, numP, 1) - randn(numP,1) * sqrt( a(3)*trans^2 + a(4)*rot1^2 + a(4)*rot2^2 );
noisy_rot2 = repmat(rot2, numP, 1)   - randn(numP,1) * sqrt( a(1)*rot2^2 + a(2)*trans^2 );

noisy_rot1=shift_angles(noisy_rot1);
noisy_rot2=shift_angles(noisy_rot2);

x = particles.allposes(:,1);
y = particles.allposes(:,2);
theta = particles.allposes(:,3);

new_x = x + noisy_trans.* cos(theta + noisy_rot1);
new_y = y + noisy_trans.* sin(theta + noisy_rot1);
new_theta = theta + noisy_rot1 + noisy_rot2;

delta_theta = new_theta - theta;

theta = mod(theta, 2*pi);

new_particle_mat = [ new_x new_y new_theta];

new_particles=particles;
new_particles.allposes=new_particle_mat;


%% Check for invalid particles that collide with walls
x_coor = round(new_x./map.resolution);
y_coor = round(new_y./map.resolution);

x_map_size = map.data_size(1);
y_map_size = map.data_size(2);
x_coor(x_coor > x_map_size) = x_map_size;
y_coor(y_coor > y_map_size) = y_map_size;
x_coor(x_coor < 1) = 1;
y_coor(y_coor < 1) = 1;

map_index = sub2ind(map.data_size, x_coor, y_coor);

invalid_particles = map.data(map_index) < free_threshold;
numInvalidParticles = sum(invalid_particles);

new_particles.allposes(invalid_particles,:) = [];
new_particles.allweights = new_particles.allweights(~invalid_particles);
new_particles.num=size(new_particles.allposes,1);
