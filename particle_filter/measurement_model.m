function weight = measurement_model(pose, z, laser_spec, sensor_model_spec, thresh_map)
% m -the number of particles
% pose - x,y,theta  size is mx3 
% Z is the incoming observation and is 1x180
% laser_spec
% senosr_model_spec - used to generate probability distribution p(z|x)
% thresh_map - thresholded map 


scan = fast_trace(pose, laser_spec, thresh_map);

%Compare this scan with the input Z to calculate weight updates

% tic
z_sampled = z(1:laser_spec.interval:end);

z_sampled(z_sampled>laser_spec.max_range)=laser_spec.max_range;
z_sampled(z<0)=0;
prob_z=compute_range_probability(z_sampled,scan,laser_spec,sensor_model_spec);
% weight_time=toc;

% fprintf('Updating weights took %d s\n',weight_time)
weight=prod(prob_z,2);
% normalized_weights=normalize_weights(weights);
%prob_z is mx180

end
