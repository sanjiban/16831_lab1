function new_angle=shift_angles(angle)
        angle=mod(angle,2*pi);
        if angle >= pi
            new_angle=angle-2*pi;
        else new_angle=angle;
        end
end