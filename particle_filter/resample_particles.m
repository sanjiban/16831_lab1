function new_particles = resample_particles(particles,desired_num)
% according to the current particle's weight, do resampling to select some
% good samples.  stochastic resampling
%  particles: including particle position and weights
%  desired_num: desired samples you want to draw.


%error: if desired_num and particles.num are different
particles.num=length(particles.allposes);

rand_ind = randperm(particles.num);
x = particles.allposes(rand_ind,:);  % why first disorder them.
w = particles.allweights(rand_ind);
w = w/sum(w);
wc  = cumsum(w);

num_to_add = 1/particles.num;
rand_number = wc(find(wc>=rand(1),1,'first'));  %find the first index to be larger than rand(1)
rand_number = repmat(rand_number,particles.num,1);
rand_number = rand_number + (1:particles.num)'*num_to_add;
rand_number  = rem(rand_number,1);
ind = zeros(particles.num,1);


for i=1:particles.num
    
   if rand_number(i)>wc(end)
       rand_number(i)=wc(end);
   end
    
   [ind(i),~] =  find(wc>=rand_number(i),1,'first');
end

xn = x(ind,:);

wanted_index = ceil(rand(desired_num,1)*particles.num);
xn = xn(wanted_index,:);

new_particles=particles;
new_particles.allposes = xn;
new_particles.allweights=ones(desired_num,1)/desired_num;
new_particles.num=length(new_particles.allposes);

end