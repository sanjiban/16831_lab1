function new_particles=motion_model(particles,oldpose,newpose,odomparam)
% move particles according to odometry motion model.
%  particles: contain all the particle weighs and poses.
%  oldpose: last odometry pose  [x y theta]   N*3
%  newpose: current odometry pse [x y theta]  1*3
%  odomparam: contain four parameters of motion model noise

delta_x = newpose(1) - oldpose(1);
delta_y = newpose(2) - oldpose(2);
delta_rot = newpose(3) - oldpose(3);

rot1 = atan2(delta_y, delta_x) - oldpose(3);
trans = sqrt((delta_x)^2 + (delta_y)^2);
rot2 = delta_rot - rot1;

rot1=shift_angles(rot1);
rot2=shift_angles(rot2);

a=odomparam;
numP = length(particles.allposes);
noisy_rot1 = repmat(rot1, numP, 1)  - randn(numP,1) * sqrt( a(1)*rot1^2 + a(2)*trans^2 );   %generate samples with variance sqrt(...)
noisy_trans = repmat(trans, numP, 1) - randn(numP,1) * sqrt( a(3)*trans^2 + a(4)*rot1^2 + a(4)*rot2^2 );
noisy_rot2 = repmat(rot2, numP, 1)   - randn(numP,1) * sqrt( a(1)*rot2^2 + a(2)*trans^2 );

noisy_rot1=shift_angles(noisy_rot1);
noisy_rot2=shift_angles(noisy_rot2);

x = particles.allposes(:,1);
y = particles.allposes(:,2);
theta = particles.allposes(:,3);

new_x = x + noisy_trans.* cos(theta + noisy_rot1);
new_y = y + noisy_trans.* sin(theta + noisy_rot1);
new_theta = theta + noisy_rot1 + noisy_rot2;

delta_theta = new_theta - theta;

theta = mod(theta, 2*pi);

new_particle_mat = [ new_x new_y new_theta];

new_particles=particles;
new_particles.allposes=new_particle_mat;

end