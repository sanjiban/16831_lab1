function particles_pose =  gen_random_particles( numParticles, map, free_threshold)
%randomly spread the particles in all the free space

[free_rows,free_cols] = find(map.data > free_threshold);  %% why?? -- because we only want particles which are unoccupied in the graph 

random_indices = randi(length(free_rows),numParticles,1);


particles_pose = [ map.resolution * free_rows(random_indices(1:numParticles))';
              map.resolution * free_cols(random_indices(1:numParticles))';
              2*pi*rand(1,numParticles)];
          
particles_pose = particles_pose';           
          
% particles = particles +[0.1*randn(1,numParticles); 0.1*randn(1,numParticles);5*(pi/180)*randn(1,numParticles)]';    &why add a randn noise      
end
