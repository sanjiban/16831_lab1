clear all; close all;

%% parameters
params.pfnum=500;
params.motion_model=[0.001 0.001 0.0001 0.0001]';   % motion model parameters
params.free_threshold = 0.8; % Cells less than this are considered free
params.occupied_threshold =0.2;  % Cells greater than this are considered occupied
params.laser_max_range = 8183.00; % Maximum laser range in meters
params.std_dev_hit = 0.2; % standard deviation error in a laser range measurement.
params.beam_model=[0.7 0.2 0.1 0.1];  % Weights for beam model [zHit zShort zMax zNoise]
params.beam_model = params.beam_model/ sum(params.beam_model);
params.lambda_short=0.1;  % calculate the chance of hitting random people or unmapped obstacles

%% store all the particle positions and weights
particles.allweights=zeros(params.pfnum,1);  %all the particle's weights    N*1
particles.allposes=zeros(params.pfnum,3);  %all the particle's poses    N*3 each row is [x y theta]
particles.meanpose=zeros(1,3);   % estimated position, weighted mean of the particles  1*3  
particles.num=length(particles.allweights);

%% read map and sensor reading log files
mapPath = 'resources/map/wean.dat';
%map = read_map(mapPath);
load('resources/processed_map/wean_map.mat');
map.data(map.data == -1) = 0.5;

figure(1);
thresholded_map=threshold_map(map, params.occupied_threshold);
visualize_map(thresholded_map);title('global map');
logPath = 'resources/log/robotdata1.log';
%[robot_log, laser_log]=read_log(logPath);
load('resources/processed_logs/robotdata1.mat');
laser_spec = get_laser_spec();

%% initiaze weights and positions based on the first laser readings.
%random sample particles in free space of whole map
particles.allposes =  gen_random_particles( params.pfnum, map, params.free_threshold);
particles.allweights= average_weights(params.pfnum);
particles.meanpose = compute_mean_pose(particles);
visualize_paricle(particles,1);

%% for loop to read each line in the log.
for k=2:length(robot_log.index)

     oldpose=robot_log.p_robot(k-1,1:3); new_pose=robot_log.p_robot(k,1:3);
     
     particles=odometry_motion_model(particles,oldpose,newpose,params.motion_model,map,params.free_threshold);

     readingtype=robot_log.sensor_index(k);
    
    if readingtype > 0   %laser reading
        
        laser_reading = laser_log.laser_scan(readingtype,1:180);  
        
        scan_set = fast_trace(particles.allposes,laser_spec,thresholded_map);  %ray casting  N*L  N is num of particles, each row is a laser measurement.
        
        particles.allweights=beam_range_finder_model(scan_set,laser_reading);  % update weights using beam sensor model  TODO by sezal
        
        particles.allweights=normalize_weights(particles.allweights);  % normalize weights, so the sum is 1

        % check some criterion determine_desiredNum();  if want adaptative resampling
        
        particles = resample_particles(particles, params.pfnum);  

        particles.mean_pose = compute_mean_pose(particles);
        
        figure(1);hold on;
        
        visualize_scan(particles.meanpose,laser_reading,laser_spec);   
    end

end
