# README #

This localizes a poor lost robot in Wean Hall. Please use this readme to learn how to use the code and develop

# Quick Start #

1. Run init_setup.m to set up paths
2. Go to examples and try any one of them out

# File Structure #

Please adhere to this file structure and not litter code.

1. examples - contains all the "executable" examples. This should contain no actual algorithms, but scripts that call the base algorithms and utils
2. particle_filter - contains the core particle filter algorithm. Dont put helper utils here. However the motion model and the sensor model should go here.
3. utils - this contains little building blocks like map reading, raycasting, visualization etc.
4. resources - contains the original data files as well as processed mat files that have read the data and stored it in our pretty structs.

# Coding Style #
1. Dont use camel-case, use underscore and keep coding consistent as much as possible.
2. Make sure you fill in a description, I/O to your functions else other ppl cant debug it.
3. Use structs and parameters wherever you can. Avoid for loops wherever you can.

# Authors #
* Sanjiban Choudhury (sanjiban@cmu.edu)
* Sezal Jain
* Shichao Yang