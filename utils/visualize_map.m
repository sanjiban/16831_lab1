function visualize_map(map)
%VISUALIZE_MAP See a map with global coordinate frame
%   Detailed explanation goes here
colormap('gray');
imagesc(map.autoshifted_x +[0 map.global_mapsize_x], map.autoshifted_y + [0 map.global_mapsize_y], map.data');
axis xy;
end

