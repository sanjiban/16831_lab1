function [ID] = coord2index(x, y, map)
%COORD2INDEX Converts x,y locations to map indices
%   x and y are vectors, map is a structure
ID = [round(((x-map.autoshifted_x)/map.resolution)+1),round(((y-map.autoshifted_y)/map.resolution)+1)];
end