function [ laser_pose ] = get_laser_pose( pose, laser_spec )
%GET_LASER_POSE Transforms from robot frame to laser frame
%   pose is robot pose, laser_spec is laser_spec struct, laser_pose is the
%   transformed pose
laser_pose = pose + [ laser_spec.body_offset(1)*cos(pose(:,3)) laser_spec.body_offset(1)*sin(pose(:,3)) zeros(size(pose,1), 1)];

end

