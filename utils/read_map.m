function map = read_map( filename )
%READ_MAP Reads a map and creates a structure with fields
%   We dont do sanity checks ;)

fid = fopen(filename);
map.global_mapsize_x = sscanf(fgetl(fid), 'robot_specifications->global_mapsize_x                 %d');
map.global_mapsize_y = sscanf(fgetl(fid), 'robot_specifications->global_mapsize_y                 %d');
map.resolution = sscanf(fgetl(fid), 'robot_specifications->resolution                       %d');
map.autoshifted_x = sscanf(fgetl(fid), 'robot_specifications->autoshifted_x                    %d');
map.autoshifted_y = sscanf(fgetl(fid), 'robot_specifications->autoshifted_y                    %d');
fgetl(fid);
map.data_size = sscanf(fgetl(fid), 'global_map[0]: %d %d');
map.data = [];
for i = 1:map.data_size(1)
    row = strread(fgetl(fid));
    map.data = [map.data; row(1:map.data_size(2))];
end
fclose(fid);
end

