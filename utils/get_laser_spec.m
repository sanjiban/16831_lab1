function laser_spec = get_laser_spec()
% gets a struct which says everything about a laser

laser_spec.max_range = 8183;
laser_spec.range_resolution = 10;
laser_spec.angle_resolution = pi/180;
laser_spec.angle_range = [-pi/2 pi/2-pi/180];
laser_spec.body_offset = [25 25];
laser_spec.interval = 2;
end

