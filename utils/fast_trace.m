function scan_set = fast_trace(pose, laser_spec, map)
%% Super fast ray trace
% pose - a set of poses where you want to do raycasting. Nx3. Each row is
% the x,y,heading of the origin of the ray. N is the number of poses we
% want to evaluate
% laser_spec - a struct (see definition file) containing laser specs
% map -  a thresholded map 
% scan_set - a matrix where each row corresponds to a measurement set at one pose and
% the columns is the laser scan. N*L

angles = laser_spec.angle_range(1):laser_spec.angle_resolution:laser_spec.angle_range(2);
angles = angles(1:laser_spec.interval:end);
laser_pose = get_laser_pose( pose, laser_spec );
r = zeros(size(laser_pose,1),size(angles,2));
x = r;
y = r;
collision = r;
ranges = [0:laser_spec.range_resolution:laser_spec.max_range]';
for j=1:size(laser_pose,1)
    angles2 = bsxfun(@plus, angles, laser_pose(j,3));
    for i=1:size(angles,2)        
        xy = bsxfun(@plus, laser_pose(j,1:2), bsxfun(@times,ranges, [cos(angles2(i)),sin(angles2(i))]));
        xy = coord2index(xy(:,1),xy(:,2),map);
        [outId1,~] = find(xy(:,1)<1|xy(:,2)<1,1,'first');
        [outId2,~] = find(xy(:,1)>=size(map.data,1)|xy(:,2)>=size(map.data,2),1,'first');

        xy(:,1) = min(size(map.data, 1)*(ones(size(xy(:,1)))), max((ones(size(xy(:,1)))), xy(:,1)));
        xy(:,2) = min(size(map.data, 2)*(ones(size(xy(:,2)))), max((ones(size(xy(:,2)))), xy(:,2)));
        ind = sub2ind(size(map.data),xy(:,1),xy(:,2));
        [collisionId,~] = find(map.data(ind)<0.5,1,'first');
        collisionId = min([collisionId;outId1;outId2]);
        if isempty(collisionId)
            reflection_range = laser_spec.max_range;
            collision_det = 0;
            xy = xy(end,:);
        else
            collisionId = max(collisionId-1,1);
            reflection_range = ranges(collisionId);
            collision_det = 1;            
            xy = xy(collisionId,:);
            [xy(:,1), xy(:,2)] = index2coord([xy(:,1) xy(:,2)],map);
        end
        
        x(j,i) = xy(1);
        y(j,i) = xy(2);
        r(j,i) = reflection_range;
        collision(j,i) = collision_det;
    end
end

scan_set = r;
