function sensor_model_spec= get_sensor_model_spec()
% these are the parameters for the sensor/measurement model
sensor_model_spec.hit_sigma=25;
sensor_model_spec.short_lambda=.01;


sensor_model_spec.w_hit=.562;%0.7;
sensor_model_spec.w_short=.21;%.0005;%.2;
sensor_model_spec.w_max=0.001;%.1;
sensor_model_spec.w_noise=0.22;%.1;

sum=sensor_model_spec.w_hit+sensor_model_spec.w_short+sensor_model_spec.w_max+sensor_model_spec.w_noise;
sensor_model_spec.w_hit=sensor_model_spec.w_hit/sum;
sensor_model_spec.w_short=sensor_model_spec.w_short/sum;
sensor_model_spec.w_max=sensor_model_spec.w_max/sum;
sensor_model_spec.w_noise=sensor_model_spec.w_noise/sum;
end
