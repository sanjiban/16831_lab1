function output = threshold_map( input, occ_threshold )
%THRESHOLD_MAP thresholds the map coz we cant scan a probabilistic map!
%  input - input map
%  occ_threshold - value below which everything is occ, above which is
%  empty

output = input;
output.data(input.data==-1) = 0.5;
output.data(output.data <= occ_threshold) = 0;
output.data(output.data > occ_threshold) = 1;
end

