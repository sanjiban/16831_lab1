function newweights=normalize_weights(weights)
% normalize the weights, so that the sum of weights is 1;
    sumall=sum(weights);
    num=length(weights);
    if sumall == 0
        newweights= 1/num*ones(num,1);
    else
        newweights = weights/sumall;
    end
    
     newweights = newweights.^(1/30);
     newweights = newweights./sum(newweights);
    
end