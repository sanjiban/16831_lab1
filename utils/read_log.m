function [robot_log, laser_log] = read_log(file_name)
%robot_log contains 
%   p_robot is odometry [x,y,theta,time]
%laser_log contains
%   p_robot_laser is the position of the robot at the time of laser reading [x,y,theta,time]
%   p_laser is the position of the laser at the time of laser reading [x,y,theta,time]
%   laser_scan is the range readings [ranges time]
%log_index  contains
%    sequence of odometry and laser readings

fid = fopen(file_name);
p_robot=[];
p_robot_laser=[];
p_laser=[];
range=[];
log_index = [];
pos_count = 1;
neg_count=-1;

tline = fgetl(fid);
while ischar(tline)
    if(tline(1)=='O')
        o = str2num(tline(2:end));
        p_robot = [p_robot;o];
        log_index = [log_index; neg_count];
        negcount=neg_count-1;
    elseif(tline(1)=='L')
        o = str2num(tline(2:end));
        p_robot = [p_robot;o(1:3),o(end)];
        p_robot_laser = [p_robot_laser;o(1:3),o(end)];
        p_laser= [p_laser;o(4:6),o(end)];
        range = [range;o(7:end-1)];
        log_index = [log_index; pos_count];
        pos_count = pos_count + 1;
    end
    tline = fgetl(fid);
end
fclose(fid);

robot_log.pos_robot = p_robot;
robot_log.sensor_index = log_index;
laser_log.pos_robot_laser = p_robot_laser;
laser_log.pos_laser_origin = p_laser;
laser_log.laser_scan = range;