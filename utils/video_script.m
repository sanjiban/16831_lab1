clc
clear all
close all

load('log4_success.mat')
mask=generate_robo_mask(.2,.3);

vidObj = VideoWriter('vid.avi');
open(vidObj);
    
figure()
for i=43:size(particle_list,2)
  %%visualize selected(highest weight) particle with a robot mask
  readingtype=robot_log.sensor_index(i);
  if readingtype<0 
      continue
  end
  cla
  visualize_map(map);
  hold on
  %%visualize all particle
  particles_=particle_list(i).particles;
  
  selected_particle=particles_.allposes(particles_.bestparticle,:);
  selected_observation= laser_log.laser_scan(readingtype,1:180);
  scatter(particles_.allposes(:,1),particles_.allposes(:,2),'m.');
  scatter(selected_particle(1),selected_particle(2),25,'b','filled');
  
  %this is for the yellow mask 
      q = [cos(selected_particle(3)/2) 0 0 sin(selected_particle(3)/2)];
      [~,max_index]= max(mask(:,1));
      laser_origin = mask(max_index,:);
      laser_origin = quatrotate(q,[laser_origin,0]);
      laser_origin(:,1) =  selected_particle(1);
      laser_origin(:,2) =  selected_particle(2);
      mask = quatrotate(q,[mask,zeros(size(mask,1),1)]);
      mask(:,1) = mask(:,1) + selected_particle(1);
      mask(:,2) = mask(:,2) + selected_particle(2);

      %%visualize observation from best particle
      angles = -pi/2:pi/180:pi/2-pi/180;
      r_vec = [cos(angles'),sin(angles'),zeros(size(angles'))];
      r_vec = quatrotate(q,r_vec);
      r_vec = repmat(selected_observation',1,3).*r_vec;
      r_vec(:,1) = r_vec(:,1);
      r_vec(:,2) = r_vec(:,2);
      r_vec(:,1) = r_vec(:,1) + laser_origin(1);
      r_vec(:,2) = r_vec(:,2) + laser_origin(2);
      r_vec = [laser_origin;r_vec;laser_origin];


      hold on
      patch(mask(:,1),mask(:,2),'b','FaceAlpha',0.7)
      patch(r_vec(:,1),r_vec(:,2),'y','FaceAlpha',0.2);
      pause(.03)
      writeVideo(vidObj,getframe);
      pause(.01)
end

close(vidObj);
