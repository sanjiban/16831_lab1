function [x,y] = index2coord(ID, map)
%COORD2INDEX Converts id to x,y position
%   ID is a vector of positions, map is a map struct
x = ID(:,1)*map.resolution+map.autoshifted_x;
y = ID(:,2)*map.resolution+map.autoshifted_y;
end