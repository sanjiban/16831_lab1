function mean_pose = compute_mean_pose(particles)
%allposes is N*3   allweights is N*1

        mean_pose = (particles.allweights)' * particles.allposes;
end
