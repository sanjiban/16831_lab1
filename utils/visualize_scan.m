function visualize_scan( pose, scan, laser_spec,color ,width)
%Visualize raycast
% pose [x y theta]  1*3


laser_pose = get_laser_pose( pose, laser_spec );
angles = (laser_spec.angle_range(1):laser_spec.angle_resolution:laser_spec.angle_range(2))';
angles = angles(1:laser_spec.interval:end);
angles = laser_pose(3) + angles;


hits = repmat(laser_pose(1:2), size(angles,1), 1) + [ scan'.*cos(angles) scan'.*sin(angles)];
data = [repmat(laser_pose(1:2), size(angles,1), 1) hits(:,1:2) ];
for i = 1:size(data, 1)
    plot([data(i, 1) data(i, 3)], [data(i, 2) data(i, 4)], color,'LineWidth',width);
    hold on;
end
plot([pose(1) pose(1)+25*cos(pose(3))], [pose(2) pose(2)+25*sin(pose(3))], 'b');
end

