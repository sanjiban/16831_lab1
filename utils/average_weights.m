function weights = average_weights(num)
% make all the weight=1/num
    weights=ones(num,1)/num;
end