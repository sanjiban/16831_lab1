clc;
clear;
close all;

%this just displays the probability model for one beam
%can adjust sensor spec model parameters based on this test
% Load laser specs
laser_spec = get_laser_spec();

% Load sensor model specs
sensor_spec = get_sensor_model_spec();

% Test sensor model
z=1:laser_spec.range_resolution:laser_spec.max_range;
z_expected = 500;%randi(laser_spec.max_range);
probability=zeros(length(z),1);
z=z';
z(length(z))=laser_spec.max_range;
tic;
for i =1:length(z)
  probability(i)=compute_range_probability(z(i),z_expected,laser_spec,sensor_spec);
end

scan_time = toc;
fprintf('Scan took %d s\n',scan_time)
% Visualize 
figure;
hold on;
plot(z,probability);
xlabel('range');
ylabel('p(z|x)');
