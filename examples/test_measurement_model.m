clc;
clear;
close all;

% Load the map
load('../resources/processed_map/wean_map.mat', 'map');
% Threshold map
occ_threshold = 0.1;
thresh_map = threshold_map( map, occ_threshold );

% Load the measurements
load('../resources/processed_logs/robotdata2.mat', 'laser_log');
% load one measurement
% the update is based on this one measurement
z=laser_log.laser_scan(1,:);
z=z(1:180);
%this just displays the probability model for one beam
%can adjust sensor spec model parameters based on this test
% Load laser specs
laser_spec = get_laser_spec();

% Load sensor model specs
sensor_spec = get_sensor_model_spec();

% test measurement model
% with m particles and one observation, update weights

% create m particles
% X is mx3
numParticles=500;
x=gen_random_particles( numParticles, map, occ_threshold);

%call measurement model
tic
scan = fast_trace(x, laser_spec, thresh_map);
scan_time = toc;
fprintf('Scan took %d s\n',scan_time)
%scan is mx180

%Compare this scan with the input Z to calculate weight updates

tic
z_sampled = z(1:laser_spec.interval:end);
prob_z=compute_range_probability(z_sampled,scan,laser_spec,sensor_spec);
weights=prod(prob_z,2);
normalized_weights=normalize_weights(weights);
weight_time=toc;
fprintf('Updating weights took %d s\n',weight_time)
[m i]=max(normalized_weights);

%visualize weight of all particles
figure;
hold on;
visualize_map(thresh_map);
scatter(x(:,1),x(:,2),25,normalized_weights/m,'filled');
scatter(x(:,1),x(:,2),36,'k');
%Visualize both observation and the scan from the highest weight to see if they are close
figure;
subplot(1,2,1)
hold on;
visualize_map(thresh_map);
visualize_scan( x(i,:), scan(i,:), laser_spec, 'r',1);
title('selected particle');
subplot(1,2,2)
hold on;
visualize_map(thresh_map);
visualize_scan( x(i,:), z_sampled, laser_spec, 'g',1);
title('input observation with pose of selected particle');
