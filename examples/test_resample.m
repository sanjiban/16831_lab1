particles.allposes=repmat(linspace(1,10,10)',[1,3]);
particles.allweights=[1 4 4 8 0 0 0 0 0 0]';
particles.num=length(particles.allweights);
desired_num=2000;

 new_particles = resample_particles(particles,desired_num);

 hist(new_particles.allposes(:,1),particles.allposes(:,1))