clc;
clear;
close all;

% if matlabpool('size') ~= 0 % checking to see if my pool is already open
%     matlabpool close
%     matlabpool open
% else
%     matlabpool open
% end

% Load the map
load('../resources/processed_map/wean_map.mat', 'map');
% Threshold map
occ_threshold = 0.1;
thresh_map = threshold_map( map, occ_threshold );

% Load the measurements
load('../resources/processed_logs/robotdata1.mat');

%% parameters
params.pfnum = 10000;
params.resampling_ration=1.0;
params.motion_model= [0.001 0.001/10^4 0.0001/10^4 0.0001]';   % motion model parameters

params.free_threshold = 0.8; % Cells less than this are considered free
params.occupied_threshold =0.2;  % Cells greater than this are considered occupied

%% store all the particle positions and weights
particles.allweights=zeros(params.pfnum,1);  %all the particle's weights    N*1
particles.allposes=zeros(params.pfnum,3);  %all the particle's poses    N*3 each row is [x y theta]
particles.meanpose=zeros(1,3);   % estimated position, weighted mean of the particles  1*3  
particles.num=length(particles.allweights);

%% initiaze weights and positions based on the first laser readings.
%random sample particles in free space of whole map
particles.allposes =  gen_random_particles( params.pfnum, map, params.free_threshold);
particles.allweights= average_weights(params.pfnum);
particles.meanpose = compute_mean_pose(particles);


% Load laser specs
laser_spec = get_laser_spec();
laser_spec_normal= laser_spec;
laser_spec_normal.interval = 1;

% Load sensor model specs
sensor_spec = get_sensor_model_spec();
particle_list=struct('particles',{});
particle_list(1).particles=particles;
figure(1); hold on;
visualize_map(map);
visualize_paricle(particles,1);

pause
useparfor=false;
count = 1;
%% for loop to read each line in the log.
for k=52:length(robot_log.sensor_index)
    oldpose=robot_log.pos_robot(k-1,1:3);
    newpose=robot_log.pos_robot(k,1:3);
    particles=odometry_motion_model(particles,oldpose,newpose,params.motion_model,map,params.free_threshold);
    fprintf('Odom took %d s\n',toc)

    
    readingtype=robot_log.sensor_index(k);
    if readingtype > 0   %laser reading
        
        figure(1);hold on;
        visualize_map(thresh_map);
        
        laser_reading = laser_log.laser_scan(readingtype,1:180);  
        
        tic    
        weights=average_weights(particles.num);
        if(useparfor)
            parfor no=1:particles.num
                weights(no) = measurement_model(particles.allposes(no,1:3), laser_reading, laser_spec, sensor_spec, thresh_map);
            end
        else
            weights = measurement_model(particles.allposes(:,1:3), laser_reading, laser_spec, sensor_spec, thresh_map);
        end
        scan_time = toc;
        fprintf('scan and weight update took %d s\n',scan_time)
        particles.allweights(:)=normalize_weights(weights(:));

        [m i]=max(particles.allweights);
        scatter(particles.allposes(i,1),particles.allposes(i,2),25,'b','filled');
        %particles.meanpose = compute_mean_pose(particles);

        scan = fast_trace(particles.allposes(i,:), laser_spec, thresh_map); 
        visualize_scan(particles.allposes(i,:),scan,laser_spec,'g'); 
        laser_reading_sampled=laser_reading(1:laser_spec.interval:end);
        visualize_scan(particles.allposes(i,:),laser_reading_sampled,laser_spec,'r');
        
        particles.bestparticle=i;
        particles.bestparticlescan=scan;
        particle_list(k).particles=particles;       
        
        
        % check some criterion determine_desiredNum();  if want adaptative resampling
        
        if (count > 20)
            params.pfnum = 1000;
        end
        
        particles = resample_particles(particles, floor(params.pfnum));
        
%         global_random_pf_num=params.pfnum-floor(params.pfnum*params.resampling_ration);
%         global_random_pf_poses =  gen_random_particles(global_random_pf_num, map, params.free_threshold);
%         particles.allposes=[particles.allposes;global_random_pf_poses];
        particles.allweights=ones(params.pfnum,1)/params.pfnum;
        particles.num=params.pfnum;
        
        scatter(particles.allposes(:,1),particles.allposes(:,2),'m.');%,25,particles.allweights(:)/m,'filled')

        %scatter(particles.allposes(:,1),particles.allposes(:,2),'g.');
        
        count = count + 1;
    end
%     figure(2);
%     hold on;
%     scatter(newpose(1)*10,newpose(2)*10,'m.');%,25,particles.allweights(:)/m,'filled')
    pause(.01);
end

%Visualize both observation and the scan from the highest weight to see if they are close

