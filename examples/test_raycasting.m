clc;
clear;
close all;

% Load the map
load('../resources/processed_map/wean_map.mat', 'map');

% Threshold map
occ_threshold = 0.1;
thresh_map = threshold_map( map, occ_threshold );

% Load laser specs
laser_spec = get_laser_spec();

% Test raycasting
pose = [5744 4596 pi/2];
tic;
scan_set = fast_trace(pose, laser_spec, thresh_map);
scan_time = toc;
fprintf('Scan took %d s\n',scan_time)

% Visualize 
figure;
hold on;
visualize_map(thresh_map);
visualize_scan( pose, scan_set, laser_spec, 'r' );