 clear all;close all;
oldpose=[0 0 0];
newpose=[100 100 pi/2];

a=-0.2;b=0.2;
yaw_a=-pi;yaw_b=pi;
N=500;
particles.allposes(1:N,1:2)=a*ones(N,2)+(b-a).*randn(N,2);
% particles.allposes(1:N,3)=yaw_a*ones(N,1)+(yaw_b-yaw_a).*rand(N,1);
particles.allposes(1:N,3)=zeros(N,1);

odomparam=[0.001 0.001/10^4 0.0001/10^4 0.0001]';

new_particles=motion_model(particles,oldpose,newpose,odomparam);

figure; hold on;
scatter(particles.allposes(:,1),particles.allposes(:,2),'r.')
scatter(new_particles.allposes(:,1),new_particles.allposes(:,2),'g.')
plot(oldpose(1),oldpose(2),'bo')
plot(newpose(1),newpose(2),'bo')
plot([oldpose(1);newpose(1)],[oldpose(2);newpose(2)],'b')
